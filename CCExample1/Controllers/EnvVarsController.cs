﻿using System;
using System.Collections;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CCExample1.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class EnvVarsController : ControllerBase
  {
    [HttpGet]
    public JsonResult Get() {
      Console.WriteLine("EnvVars");
      return new JsonResult(
        Environment.GetEnvironmentVariables().
          Cast<DictionaryEntry>().
          OrderBy(de => de.Key).
          Select(de => $"{de.Key}={de.Value}"),
        new JsonSerializerSettings { Formatting = Formatting.Indented }
      ) { StatusCode = StatusCodes.Status200OK };
    }
  }
}
