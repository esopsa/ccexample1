﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace CCExample1.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class VersionController : ControllerBase
  {
    [HttpGet]
    public string Get() {
      Console.WriteLine("Version");
      return Program.Version;
    }
  }
}
