﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace CCExample1.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class EnvVarController : ControllerBase
  {
    // GET: EnvVar/PATH
    [HttpGet("{name}")]
    public string Get(string name) {
      Console.WriteLine("EnvVar");
      if (string.IsNullOrWhiteSpace(name))
        return "<empty name>";
      string value = Environment.GetEnvironmentVariable(name);
      if (value == null)
        return $"<'{name}' not found>";
      return value;
    }
  }
}
