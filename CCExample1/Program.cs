﻿using System;
using System.Threading;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace CCExample1
{
  public static class Program
  {
    internal const string Version = "v3.4.5";

    public static void Main(string[] args) {
      Console.WriteLine($"CCExample1 ({Version}) started");
      //Thread t = new Thread(() => {
      //  while (true) {
      //    Console.WriteLine(DateTime.Now.ToString("U"));
      //    Thread.Sleep(2000);
      //  }
      //});
      //t.Start();
      CreateWebHostBuilder(args).Build().Run();
      //t.Abort();
      Console.WriteLine($"CCExample1 ({Version}) stopped");
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
      WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .ConfigureKestrel(options => options.ListenAnyIP(5000))
    ;
  }
}
